from defines import *


class ParsingTree:
    """
    This class represents the parsing
    tree of a given expression the tree
    holds its root which points to the left
    and right subtrees
    """
    class Node:
        """
        This class represents a tree node
        it holds a token which is an operator,
         parenthesis or a number (stored as a string)
        """
        def __init__(self, parent=None, tok=None):
            self.token = tok
            self.parent = parent
            self.left = None
            self.right = None

        def __repr__(self):
            print "(%s)" % self.token

        def __str__(self):
            return "(%s)" % self.token

        # evaluate the expression which is represented
        # by the subtree that starts at this node
        def evaluate(self):
            if self.token.isdigit():
                return int(self.token)
            elif self.token in ParsingTree.binary_operators:
                return operations[self.token](self.left.evaluate(), self.right.evaluate())
            elif self.token in ParsingTree.unary_operators:
                return operations[self.token](self.left.evaluate())

    binary_operators = ["+", "-", "*", "/", "^", "%", "@", "$", "&"]
    unary_operators = ["~", "!"]

    def __init__(self):
        self.root = ParsingTree.Node()

    # evaluate the expression that is represented
    # by the parsing tree
    def evaluate(self):
        return self.root.evaluate()




