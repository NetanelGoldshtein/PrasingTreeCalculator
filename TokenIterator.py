from Utility import is_number
from defines import *


class TokenIterator:
    """
    this class is an iterator for an expression
    it returns the expression tokens one by one
    and enables "peek" the next token without increasing
    the inner index
    """

    def __init__(self, expression):
        self.token_list = self.get_token_list(expression)
        self.index = 0

    def __iter__(self):
        return self

    @staticmethod
    def get_token_list(expression):
        token_list = []
        token = ""
        for c in expression:
            print c
            if c == " ":
                continue
            elif c.isdigit():
                token += c
            else:
                token_list.append(token)
                token_list.append(c)
                token = ""
        if is_number(token):
            token_list.append(token)
        logger.debug(token_list)
        return token_list

    def next(self):
        if self.index < len(self.token_list):
            token = self.token_list[self.index]
            self.index += 1
            return token
        return "iterator_end"

    def peek(self, dist=0):
        if self.index + dist < len(self.token_list):
            return self.token_list[self.index + dist]
        return "iterator_end"

    # increases the inner index and returns
    # the next token
    def expect(self, token):
        if self.next() != token:
            strerror = "expected %s" % token
            raise RuntimeError(strerror)

    # return the next token (without moving the stream)
    def erase(self, dist=0):
        del self.token_list[self.index + dist]
