from ParsingTree import *
from TokenIterator import TokenIterator


class TreeFactory:
    def __init__(self):
        pass

    @staticmethod
    def make_node(op, left_tree, right_tree=None):
        parsing_tree = ParsingTree()
        parsing_tree.root.token = op
        parsing_tree.root.left = left_tree
        parsing_tree.root.right = right_tree
        return parsing_tree

    # returns a sub-parsing tree, with op as the root token
    # and left and right subtrees as left_tree and right_tree (parameters)

    @staticmethod
    def make_leaf(token):
        leaf = ParsingTree()
        leaf.root.token = token
        return leaf

    # make a subtree which represents a leaf of the
    # parsing tree, with a number as the token and
    # no children (left and right equals None)

    # factory function for parsing tree constructs
    # a parsing tree from the given expression
    @staticmethod
    def make_tree(expression):
        token_iterator = TokenIterator(expression)
        t = TreeFactory.expr(0, token_iterator)
        try:
            token_iterator.expect("iterator_end")
            return t

        except RuntimeError as e:
            print e

    @staticmethod
    def expr(p, token_iterator):
        left_tree = TreeFactory.parse_literal(token_iterator)
        while token_iterator.peek() in ParsingTree.binary_operators and \
                precedence[token_iterator.peek()] >= p:
            op = token_iterator.next()
            logger.debug("op: " + op)
            q = precedence[op] + 1 if op != "^" else precedence[op]
            right_tree = TreeFactory.expr(q, token_iterator)
            left_tree = TreeFactory.make_node(op, left_tree, right_tree)
        return left_tree

    @staticmethod
    def parse_literal(token_iterator):
        logger.debug("index: " + str(token_iterator.index))
        logger.debug("peek: " + token_iterator.peek())
        if token_iterator.peek() in ParsingTree.unary_operators:
            op = token_iterator.next()
            q = precedence[op]
            tree = TreeFactory.expr(q, token_iterator)
            return TreeFactory.make_node(op, tree)
        elif token_iterator.peek(1) in ParsingTree.unary_operators:
            op = token_iterator.peek(1)
            token_iterator.erase(1)
            q = precedence[op]
            tree = TreeFactory.expr(q, token_iterator)
            return TreeFactory.make_node(op, tree)
        elif token_iterator.peek() == "(":
            token_iterator.next()
            tree = TreeFactory.expr(0, token_iterator)
            token_iterator.expect(")")
            return tree
        elif token_iterator.peek().isdigit():
            tree = TreeFactory.make_leaf(token_iterator.next())
            return tree
        else:
            raise RuntimeError("error")
