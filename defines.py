import math
import logging
from BasicOperators import *

operations = {"+": add, "-": sub, "*": mult, "/": div,\
              "^": math.pow, "~": neg, "i": math.fmod,\
              "!": math.factorial, "@": avg, "$": max, "&": min}

precedence = {"+": 1, "-": 1, "*": 2, "/": 2, "^": 3, \
              "~": 6, "%": 4, "!": 4, "@": 5, "$": 5, "&": 5}

# set up logger for project
logging.basicConfig()
logger = logging.getLogger("TreeLogger")
logger.setLevel("ERROR")
