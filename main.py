from TreeFactory import *
from Utility import get_expression


def main():
    expression = get_expression()
    parsing_tree = TreeFactory.make_tree(expression)
    print parsing_tree.evaluate()


if __name__ == "__main__":
    main()
